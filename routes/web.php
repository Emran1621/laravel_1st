<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Backend.................

Route::get('/dash', function () {
    return view('backend/dashboard');
});

Route::get('/table', function () {
    return view('backend/table');
});

Route::get('/from', function () {
    return view('backend/from');
});

Route::get('/log', function () {
    return view('backend/log');
});








Route::get('/login', function () {
    return view('backend/login');
});

//Frontend....................

Route::get('/homepage', function () {
    return view('frontend/homepage');
});

Route::get('/cart', function () {
    return view('frontend/cart');
});

Route::get('/category', function () {
    return view('frontend/category');
});

Route::get('/checkout', function () {
    return view('frontend/checkout');
});

Route::get('/order', function () {
    return view('frontend/order');
});

Route::get('/thanks', function () {
    return view('frontend/thanks');
});

Route::get('/details', function () {
    return view('frontend/productdetails');
});







