<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

	<title>Riode - Ultimate eCommerce Template</title>

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Riode - Ultimate eCommerce Template">
	<meta name="author" content="D-THEMES">

	<!-- Favicon -->
	<link rel="icon" type="image/png" href="frontend/images/icons/favicon.png">

	<script>
		WebFontConfig = {
			google: { families: [ 'Poppins:400,500,600,700' ] }
		};
		( function ( d ) {
			var wf = d.createElement( 'script' ), s = d.scripts[ 0 ];
			wf.src = 'js/webfont.js';
			wf.async = true;
			s.parentNode.insertBefore( wf, s );
		} )( document );
	</script>


	<link rel="stylesheet" type="text/css" href="frontend/vendor/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="frontend/vendor/animate/animate.min.css">

	<!-- Plugins CSS File -->
	<link rel="stylesheet" type="text/css" href="frontend/vendor/magnific-popup/magnific-popup.min.css">
	<link rel="stylesheet" type="text/css" href="frontend/vendor/owl-carousel/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="frontend/vendor/nouislider/nouislider.min.css">

	<link rel="stylesheet" type="text/css" href="frontend/vendor/sticky-icon/stickyicon.css">

	<!-- Main CSS File -->
	<link rel="stylesheet" type="text/css" href="frontend/css/style.min.css">
</head>

<body>
	<div class="page-wrapper">
	<x-frontend.layouts.partials.header/>

	</div>
		<!-- End Header -->
		<main class="main">
			<div class="page-header"
				style="background-image: url('images/shop/page-header-back.jpg'); background-color: #3C63A4;">
				<h3 class="page-subtitle">Categories</h3>
				<h1 class="page-title">Classic Filter</h1>
				<ul class="breadcrumb">
					<li><a href="demo1.html"><i class="d-icon-home"></i></a></li>
					<li class="delimiter">/</li>
					<li><a href="shop.html">Categories</a></li>
					<li class="delimiter">/</li>
					<li>Classic Filter</li>
				</ul>
			</div>
			<!-- End PageHeader -->
			<div class="page-content mb-10 pb-6">
				<div class="container">
					<div class="row gutter-lg main-content-wrap">
						<aside
							class="col-lg-3 sidebar sidebar-fixed sidebar-toggle-remain shop-sidebar sticky-sidebar-wrapper">
							<div class="sidebar-overlay"></div>
							<a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
							<a href="#" class="sidebar-toggle">
								<i class="fas fa-chevron-right"></i>
							</a>
							<div class="sidebar-content">
								<div class="sticky-sidebar" data-sticky-options="{'top': 10}">
									<div class="widget widget-collapsible">
										<h3 class="widget-title">All Categories</h3>
										<ul class="widget-body filter-items search-ul">
											<li><a href="#">Accessosries</a></li>
											<li>
												<a href="#">Bags</a>
												<ul>
													<li><a href="#">Backpacks & Fashion Bags</a></li>
												</ul>
											</li>
											<li>
												<a href="#">Electronics</a>
												<ul>
													<li><a href="#">Computer</a></li>
													<li><a href="#">Gaming & Accessosries</a></li>
												</ul>
											</li>
											<li><a href="#">For Fitness</a></li>
											<li><a href="#">Home & Kitchen</a></li>
											<li><a href="#">Men's</a></li>
											<li><a href="#">Shoes</a></li>
											<li><a href="#">Sporting Goods</a></li>
											<li><a href="#">Summer Season's</a></li>
											<li><a href="#">Travel & Clothing</a></li>
											<li><a href="#">Watches</a></li>
											<li><a href="#">Women’s</a></li>
										</ul>
									</div>
									<div class="widget widget-collapsible">
										<h3 class="widget-title">Filter by Price</h3>
										<div class="widget-body mt-3">
											<form action="#">
												<div class="filter-price-slider"></div>

												<div class="filter-actions">
													<div class="filter-price-text mb-4">Price:
														<span class="filter-price-range"></span>
													</div>
													<button type="submit"
														class="btn btn-dark btn-filter btn-rounded">Filter</button>
												</div>
											</form><!-- End Filter Price Form -->
										</div>
									</div>
									<div class="widget widget-collapsible">
										<h3 class="widget-title">Size</h3>
										<ul class="widget-body filter-items">
											<li><a href="#">Extra Large</a></li>
											<li><a href="#">Large</a></li>
											<li><a href="#">Medium</a></li>
											<li><a href="#">Small</a></li>
										</ul>
									</div>
									<div class="widget widget-collapsible">
										<h3 class="widget-title">Color</h3>
										<ul class="widget-body filter-items">
											<li><a href="#">Black</a></li>
											<li><a href="#">Blue</a></li>
											<li><a href="#">Green</a></li>
											<li><a href="#">White</a></li>
										</ul>
									</div>
									<div class="widget widget-collapsible">
										<h3 class="widget-title">Brands</h3>
										<ul class="widget-body filter-items">
											<li><a href="#">Cinderella</a></li>
											<li><a href="#">Comedy</a></li>
											<li><a href="#">Rightcheck</a></li>
											<li><a href="#">SkillStar</a></li>
											<li><a href="#">SLS</a></li>
										</ul>
									</div>
								</div>
							</div>
						</aside>
						<div class="col-lg-9 main-content">
							<nav class="toolbox  sticky-toolbox sticky-content fix-top">
								<div class="toolbox-left">
									<div class="toolbox-item toolbox-sort select-box text-dark">
										<label>Sort By :</label>
										<select name="orderby" class="form-control">
											<option value="default">Default</option>
											<option value="popularity" selected="selected">Most Popular</option>
											<option value="rating">Average rating</option>
											<option value="date">Latest</option>
											<option value="price-low">Sort forward price low</option>
											<option value="price-high">Sort forward price high</option>
											<option value="">Clear custom sort</option>
										</select>
									</div>
								</div>
								<div class="toolbox-right">
									<div class="toolbox-item toolbox-show select-box text-dark">
										<label>Show :</label>
										<select name="count" class="form-control">
											<option value="12">12</option>
											<option value="24">24</option>
											<option value="36">36</option>
										</select>
									</div>
									<div class="toolbox-item toolbox-layout mr-lg-0">
										<a href="shop-list-mode.html" class="d-icon-mode-list btn-layout"></a>
										<a href="shop.html" class="d-icon-mode-grid btn-layout active"></a>
									</div>
								</div>
							</nav>
							<div class="row cols-2 cols-sm-3 product-wrapper">
								<div class="product-wrap">
									<div class="product">
										<figure class="product-media">
											<a href="product.html">
												<img src="frontend/images/shop/13.jpg" alt="product" width="280" height="315">
											</a>
											<div class="product-label-group">
												<label class="product-label label-new">new</label>
											</div>
											<!-- <div class="product-action-vertical">
												<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
													data-target="#addCartModal" title="Add to cart"><i
														class="d-icon-bag"></i></a>
												<a href="#" class="btn-product-icon btn-wishlist"
													title="Add to wishlist"><i class="d-icon-heart"></i></a>
											</div> -->
											<div class="product-action">
												<a href="cart.html" class="btn-product btn-quickview" title="Quick View">Quick
													View</a>
											</div>
										</figure>
										<div class="product-details">
											<div class="product-cat">
												<a href="shop-classic-filter.html">Clothing</a>
											</div>
											<h3 class="product-name">
												<a href="product.html">Coast Pool Comfort Jacket</a>
											</h3>
											<div class="product-price">
												<ins class="new-price">$199.00</ins><del class="old-price">$210.00</del>
											</div>
											<div class="ratings-container">
												<div class="ratings-full">
													<span class="ratings" style="width:100%"></span>
													<span class="tooltiptext tooltip-top"></span>
												</div>
												<a href="product.html" class="rating-reviews">( 6 reviews )</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-wrap">
									<div class="product">
										<figure class="product-media">
											<a href="product.html">
												<img src="frontend/images/shop/1.jpg" alt="product" width="280" height="315">
											</a>
											<div class="product-action-vertical">
												<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
													data-target="#addCartModal" title="Add to cart"><i
														class="d-icon-bag"></i></a>
												<a href="#" class="btn-product-icon btn-wishlist"
													title="Add to wishlist"><i class="d-icon-heart"></i></a>
											</div>
											<div class="product-action">
												<a href="" class="btn-product btn-quickview" title="Quick View">Quick
													View</a>
											</div>
										</figure>
										<div class="product-details">
											<div class="product-cat">
												<a href="shop-classic-filter.html">bags</a>
											</div>
											<h3 class="product-name">
												<a href="product.html">Fashionable Hooded Coat</a>
											</h3>
											<div class="product-price">
												<span class="price">$35.00</span>
											</div>
											<div class="ratings-container">
												<div class="ratings-full">
													<span class="ratings" style="width:100%"></span>
													<span class="tooltiptext tooltip-top"></span>
												</div>
												<a href="product.html" class="rating-reviews">( 6 reviews )</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-wrap">
									<div class="product">
										<figure class="product-media">
											<a href="product.html">
												<img src="frontend/images/shop/2.jpg" alt="product" width="280" height="315">
											</a>

											<div class="product-label-group">
												<label class="product-label label-sale">27% off</label>
											</div>
											<div class="product-action-vertical">
												<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
													data-target="#addCartModal" title="Add to cart"><i
														class="d-icon-bag"></i></a>
												<a href="#" class="btn-product-icon btn-wishlist"
													title="Add to wishlist"><i class="d-icon-heart"></i></a>
											</div>
											<div class="product-action">
												<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
													View</a>
											</div>
										</figure>
										<div class="product-details">
											<div class="product-cat">
												<a href="shop-classic-filter.html">bags</a>
											</div>
											<h3 class="product-name">
												<a href="product.html">Women's Fashion Handbag</a>
											</h3>
											<div class="product-price">
												<span class="price">$19.00</span>
											</div>
											<div class="ratings-container">
												<div class="ratings-full">
													<span class="ratings" style="width:100%"></span>
													<span class="tooltiptext tooltip-top"></span>
												</div>
												<a href="product.html" class="rating-reviews">( 6 reviews )</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-wrap">
									<div class="product">
										<figure class="product-media">
											<a href="product.html">
												<img src="frontend/images/shop/4.jpg" alt="product" width="280" height="315">
											</a>
											<div class="product-action-vertical">
												<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
													data-target="#addCartModal" title="Add to cart"><i
														class="d-icon-bag"></i></a>
												<a href="#" class="btn-product-icon btn-wishlist"
													title="Add to wishlist"><i class="d-icon-heart"></i></a>
											</div>
											<div class="product-action">
												<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
													View</a>
											</div>
										</figure>
										<div class="product-details">
											<div class="product-cat">
												<a href="shop-classic-filter.html">Clothing</a>
											</div>
											<h3 class="product-name">
												<a href="product.html">Fashionable Padded Jacket</a>
											</h3>
											<div class="product-price">
												<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
											</div>
											<div class="ratings-container">
												<div class="ratings-full">
													<span class="ratings" style="width:100%"></span>
													<span class="tooltiptext tooltip-top"></span>
												</div>
												<a href="product.html" class="rating-reviews">( 6 reviews )</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-wrap">
									<div class="product">
										<figure class="product-media">
											<a href="product.html">
												<img src="frontend/images/shop/14.jpg" alt="product" width="280" height="315">
											</a>
											<div class="product-action-vertical">
												<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
													data-target="#addCartModal" title="Add to cart"><i
														class="d-icon-bag"></i></a>
												<a href="#" class="btn-product-icon btn-wishlist"
													title="Add to wishlist"><i class="d-icon-heart"></i></a>
											</div>
											<div class="product-action">
												<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
													View</a>
											</div>
										</figure>
										<div class="product-details">
											<div class="product-cat">
												<a href="shop-classic-filter.html">bags</a>
											</div>
											<h3 class="product-name">
												<a href="product.html">Cavin Fashion Suede Handbag</a>
											</h3>
											<div class="product-price">
												<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
											</div>
											<div class="ratings-container">
												<div class="ratings-full">
													<span class="ratings" style="width:100%"></span>
													<span class="tooltiptext tooltip-top"></span>
												</div>
												<a href="product.html" class="rating-reviews">( 6 reviews )</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-wrap">
									<div class="product">
										<figure class="product-media">
											<a href="product.html">
												<img src="frontend/images/shop/3.jpg" alt="product" width="280" height="315">
											</a>
											<div class="product-action-vertical">
												<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
													data-target="#addCartModal" title="Add to cart"><i
														class="d-icon-bag"></i></a>
												<a href="#" class="btn-product-icon btn-wishlist"
													title="Add to wishlist"><i class="d-icon-heart"></i></a>
											</div>
											<div class="product-action">
												<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
													View</a>
											</div>
										</figure>
										<div class="product-details">
											<div class="product-cat">
												<a href="shop-classic-filter.html">shoes</a>
											</div>
											<h3 class="product-name">
												<a href="product.html">Women's Fashion Hood</a>
											</h3>
											<div class="product-price">
												<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
											</div>
											<div class="ratings-container">
												<div class="ratings-full">
													<span class="ratings" style="width:100%"></span>
													<span class="tooltiptext tooltip-top"></span>
												</div>
												<a href="product.html" class="rating-reviews">( 6 reviews )</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-wrap">
									<div class="product">
										<figure class="product-media">
											<a href="product.html">
												<img src="frontend/images/shop/5.jpg" alt="product" width="280" height="315">
											</a>
											<div class="product-action-vertical">
												<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
													data-target="#addCartModal" title="Add to cart"><i
														class="d-icon-bag"></i></a>
												<a href="#" class="btn-product-icon btn-wishlist"
													title="Add to wishlist"><i class="d-icon-heart"></i></a>
											</div>
											<div class="product-action">
												<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
													View</a>
											</div>
										</figure>
										<div class="product-details">
											<div class="product-cat">
												<a href="shop-classic-filter.html">Watches</a>
											</div>
											<h3 class="product-name">
												<a href="product.html">Converse Blue Training Shoes</a>
											</h3>
											<div class="product-price">
												<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
											</div>
											<div class="ratings-container">
												<div class="ratings-full">
													<span class="ratings" style="width:100%"></span>
													<span class="tooltiptext tooltip-top"></span>
												</div>
												<a href="product.html" class="rating-reviews">( 6 reviews )</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-wrap">
									<div class="product">
										<figure class="product-media">
											<a href="product.html">
												<img src="frontend/images/shop/12.jpg" alt="product" width="280" height="315">
											</a>
											<div class="product-action-vertical">
												<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
													data-target="#addCartModal" title="Add to cart"><i
														class="d-icon-bag"></i></a>
												<a href="#" class="btn-product-icon btn-wishlist"
													title="Add to wishlist"><i class="d-icon-heart"></i></a>
											</div>
											<div class="product-action">
												<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
													View</a>
											</div>
										</figure>
										<div class="product-details">
											<div class="product-cat">
												<a href="shop-classic-filter.html">Watches</a>
											</div>
											<h3 class="product-name">
												<a href="product.html">Beyond OTP Jacket</a>
											</h3>
											<div class="product-price">
												<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
											</div>
											<div class="ratings-container">
												<div class="ratings-full">
													<span class="ratings" style="width:100%"></span>
													<span class="tooltiptext tooltip-top"></span>
												</div>
												<a href="product.html" class="rating-reviews">( 6 reviews )</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-wrap">
									<div class="product">
										<figure class="product-media">
											<a href="product.html">
												<img src="frontend/images/shop/15.jpg" alt="product" width="280" height="315">
											</a>
											<div class="product-action-vertical">
												<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
													data-target="#addCartModal" title="Add to cart"><i
														class="d-icon-bag"></i></a>
												<a href="#" class="btn-product-icon btn-wishlist"
													title="Add to wishlist"><i class="d-icon-heart"></i></a>
											</div>
											<div class="product-action">
												<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
													View</a>
											</div>
										</figure>
										<div class="product-details">
											<div class="product-cat">
												<a href="shop-classic-filter.html">Women’s</a>
											</div>
											<h3 class="product-name">
												<a href="product.html">Fashion Overnight Bag</a>
											</h3>
											<div class="product-price">
												<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
											</div>
											<div class="ratings-container">
												<div class="ratings-full">
													<span class="ratings" style="width:100%"></span>
													<span class="tooltiptext tooltip-top"></span>
												</div>
												<a href="product.html" class="rating-reviews">( 6 reviews )</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-wrap">
									<div class="product">
										<figure class="product-media">
											<a href="product.html">
												<img src="frontend/images/shop/7.jpg" alt="product" width="280" height="315">
											</a>
											<div class="product-action-vertical">
												<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
													data-target="#addCartModal" title="Add to cart"><i
														class="d-icon-bag"></i></a>
												<a href="#" class="btn-product-icon btn-wishlist"
													title="Add to wishlist"><i class="d-icon-heart"></i></a>
											</div>
											<div class="product-action">
												<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
													View</a>
											</div>
										</figure>
										<div class="product-details">
											<div class="product-cat">
												<a href="shop-classic-filter.html">shoes</a>
											</div>
											<h3 class="product-name">
												<a href="product.html">Fashion Brown Suede Shoes</a>
											</h3>
											<div class="product-price">
												<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
											</div>
											<div class="ratings-container">
												<div class="ratings-full">
													<span class="ratings" style="width:100%"></span>
													<span class="tooltiptext tooltip-top"></span>
												</div>
												<a href="product.html" class="rating-reviews">( 6 reviews )</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-wrap">
									<div class="product">
										<figure class="product-media">
											<a href="product.html">
												<img src="frontend/images/shop/8.jpg" alt="product" width="280" height="315">
											</a>
											<div class="product-action-vertical">
												<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
													data-target="#addCartModal" title="Add to cart"><i
														class="d-icon-bag"></i></a>
												<a href="#" class="btn-product-icon btn-wishlist"
													title="Add to wishlist"><i class="d-icon-heart"></i></a>
											</div>
											<div class="product-action">
												<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
													View</a>
											</div>
										</figure>
										<div class="product-details">
											<div class="product-cat">
												<a href="shop-classic-filter.html">Women’s</a>
											</div>
											<h3 class="product-name">
												<a href="product.html">Men's Fashion Jacket</a>
											</h3>
											<div class="product-price">
												<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
											</div>
											<div class="ratings-container">
												<div class="ratings-full">
													<span class="ratings" style="width:100%"></span>
													<span class="tooltiptext tooltip-top"></span>
												</div>
												<a href="product.html" class="rating-reviews">( 6 reviews )</a>
											</div>
										</div>
									</div>
								</div>
								<div class="product-wrap">
									<div class="product">
										<figure class="product-media">
											<a href="product.html">
												<img src="frontend/images/shop/9.jpg" alt="product" width="280" height="315">
											</a>
											<div class="product-action-vertical">
												<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
													data-target="#addCartModal" title="Add to cart"><i
														class="d-icon-bag"></i></a>
												<a href="#" class="btn-product-icon btn-wishlist"
													title="Add to wishlist"><i class="d-icon-heart"></i></a>
											</div>
											<div class="product-action">
												<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
													View</a>
											</div>
										</figure>
										<div class="product-details">
											<div class="product-cat">
												<a href="shop-classic-filter.html">electronics</a>
											</div>
											<h3 class="product-name">
												<a href="product.html">Fashion Cowboy Hat</a>
											</h3>
											<div class="product-price">
												<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
											</div>
											<div class="ratings-container">
												<div class="ratings-full">
													<span class="ratings" style="width:100%"></span>
													<span class="tooltiptext tooltip-top"></span>
												</div>
												<a href="product.html" class="rating-reviews">( 6 reviews )</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<nav class="toolbox toolbox-pagination">
								<p class="show-info">Showing <span>12 of 56</span> Products</p>
								<ul class="pagination">
									<li class="page-item disabled">
										<a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1"
											aria-disabled="true">
											<i class="d-icon-arrow-left"></i>Prev
										</a>
									</li>
									<li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a>
									</li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item page-item-dots"><a class="page-link" href="#">6</a></li>
									<li class="page-item">
										<a class="page-link page-link-next" href="#" aria-label="Next">
											Next<i class="d-icon-arrow-right"></i>
										</a>
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
			
		</main>
		<!-- End Main -->
		
		<x-frontend.layouts.partials.footer/>

		<!-- End Footer -->
	</div>
	<!-- Sticky Footer -->
	<div class="sticky-footer sticky-content fix-bottom">
		<a href="demo1.html" class="sticky-link">
			<i class="d-icon-home"></i>
			<span>Home</span>
		</a>
		<a href="shop.html" class="sticky-link">
			<i class="d-icon-volume"></i>
			<span>Categories</span>
		</a>
		<a href="wishlist.html" class="sticky-link">
			<i class="d-icon-heart"></i>
			<span>Wishlist</span>
		</a>
		<a href="account.html" class="sticky-link">
			<i class="d-icon-user"></i>
			<span>Account</span>
		</a>
		<div class="header-search hs-toggle dir-up">
			<a href="#" class="search-toggle sticky-link">
				<i class="d-icon-search"></i>
				<span>Search</span>
			</a>
			<form action="#" class="input-wrapper">
				<input type="text" class="form-control" name="search" autocomplete="off"
					placeholder="Search your keyword..." required />
				<button class="btn btn-search" type="submit">
					<i class="d-icon-search"></i>
				</button>
			</form>
		</div>
	</div>
	<!-- Scroll Top -->
	<a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="d-icon-arrow-up"></i></a>

	<!-- MobileMenu -->
	<div class="mobile-menu-wrapper">
		<div class="mobile-menu-overlay">
		</div>
		<!-- End Overlay -->
		<a class="mobile-menu-close" href="#"><i class="d-icon-times"></i></a>
		<!-- End CloseButton -->
		<div class="mobile-menu-container scrollable">
			<form action="#" class="input-wrapper">
				<input type="text" class="form-control" name="search" autocomplete="off"
					placeholder="Search your keyword..." required />
				<button class="btn btn-search" type="submit">
					<i class="d-icon-search"></i>
				</button>
			</form>
			<!-- End Search Form -->
			<ul class="mobile-menu mmenu-anim">
				<li>
					<a href="demo1.html">Home</a>
				</li>
				<li>
					<a href="shop.html" class="active">Categories</a>
					<ul>
						<li>
							<a href="#">
								Variations 1
							</a>
							<ul>
								<li><a href="shop-classic-filter.html">Classic Filter</a></li>
								<li><a href="shop-left-toggle-sidebar.html">Left Toggle Filter</a></li>
								<li><a href="shop-right-toggle-sidebar.html">Right Toggle Sidebar</a></li>
								<li><a href="shop-horizontal-filter.html">Horizontal Filter </a>
								</li>
								<li><a href="shop-navigation-filter.html">Navigation Filter</a></li>

								<li><a href="shop-off-canvas-filter.html">Off-Canvas Filter </a></li>
								<li><a href="shop-top-banner.html">Top Banner</a></li>
								<li><a href="shop-inner-top-banner.html">Inner Top Banner</a></li>
								<li><a href="shop-with-bottom-block.html">With Bottom Block</a></li>
								<li><a href="shop-category-in-page-header.html">Category In Page Header</a>
							</ul>
						</li>
						<li>
							<a href="#">
								Variations 2
							</a>
							<ul>
								<li><a href="shop-grid-3cols.html">3 Columns Mode</a></li>
								<li><a href="shop-grid-4cols.html">4 Columns Mode</a></li>
								<li><a href="shop-grid-5cols.html">5 Columns Mode</a></li>
								<li><a href="shop-grid-6cols.html">6 Columns Mode</a></li>
								<li><a href="shop-grid-7cols.html">7 Columns Mode</a></li>
								<li><a href="shop-grid-8cols.html">8 Columns Mode</a></li>
								<li><a href="shop-list-mode.html">List Mode</a></li>
								<li><a href="shop-pagination.html">Pagination</a></li>
								<li><a href="shop-infinite-ajaxscroll.html">Infinite Ajaxscroll </a></li>
								<li><a href="shop-loadmore-button.html">Loadmore Button</a></li>
							</ul>
						</li>
						<li>
							<a href="#">
								Variations 3
							</a>
							<ul>
								<li><a href="shop-category-grid-shop.html">Category Grid Shop</a></li>
								<li><a href="shop-category+products.html">Category + Products</a></li>
								<li><a href="shop-default-1.html">Shop Default 1 </a>
								</li>
								<li><a href="shop-default-2.html">Shop Default 2</a></li>
								<li><a href="shop-default-3.html">Shop Default 3</a></li>
								<li><a href="shop-default-4.html">Shop Default 4</a></li>
								<li><a href="shop-default-5.html">Shop Default 5</a></li>
								<li><a href="shop-default-6.html">Shop Default 6</a></li>
								<li><a href="shop-default-7.html">Shop Default 7</a></li>
								<li><a href="shop-default-8.html">Shop Default 8</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li>
					<a href="product.html">Products</a>
					<ul>
						<li>
							<a href="#">Product Pages</a>
							<ul>
								<li><a href="product-simple.html">Simple Product</a></li>
								<li><a href="product-featured.html">Featured &amp; On Sale</a></li>
								<li><a href="product.html">Variable Product</a></li>
								<li><a href="product-variable-swatch.html">Variation Swatch
										Product</a></li>
								<li><a href="product-grouped.html">Grouped Product </a></li>
								<li><a href="product-external.html">External Product</a></li>
								<li><a href="product-in-stock.html">In Stock Product</a></li>
								<li><a href="product-out-stock.html">Out of Stock Product</a></li>
								<li><a href="product-upsell.html">Upsell Products</a></li>
								<li><a href="product-cross-sell.html">Cross Sell Products</a></li>
							</ul>
						</li>
						<li>
							<a href="#">Product Layouts</a>
							<ul>
								<li><a href="product-vertical.html">Vertical Thumb</a></li>
								<li><a href="product-horizontal.html">Horizontal Thumb</a></li>
								<li><a href="product-gallery.html">Gallery Type</a></li>
								<li><a href="product-grid.html">Grid Images</a></li>
								<li><a href="product-masonry.html">Masonry Images</a></li>
								<li><a href="product-sticky.html">Sticky Info</a></li>
								<li><a href="product-sticky-both.html">Left & Right Sticky</a></li>
								<li><a href="product-left-sidebar.html">With Left Sidebar</a></li>
								<li><a href="product-right-sidebar.html">With Right Sidebar</a></li>
								<li><a href="product-full.html">Full Width Layout </a></li>
							</ul>
						</li>
						<li>
							<a href="#">Product Features</a>
							<ul>
								<li><a href="product-sale.html">Sale Countdown</a></li>
								<li><a href="product-hurryup.html">Hurry Up Notification </a></li>
								<li><a href="product-attribute-guide.html">Attribute Guide </a></li>
								<li><a href="product-sticky-cart.html">Add Cart Sticky</a></li>
								<li><a href="product-thumbnail-label.html">Labels on Thumbnail</a>
								</li>
								<li><a href="product-more-description.html">More Description
										Tabs</a></li>
								<li><a href="product-accordion-data.html">Data In Accordion</a></li>
								<li><a href="product-tabinside.html">Data Inside</a></li>
								<li><a href="product-video.html">Video Thumbnail </a>
								</li>
								<li><a href="product-360-degree.html">360 Degree Thumbnail </a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li>
					<a href="#">Pages</a>
					<ul>
						<li><a href="about-us.html">About</a></li>
						<li><a href="contact-us.html">Contact Us</a></li>
						<li><a href="account.html">Login</a></li>
						<li><a href="faq.html">FAQs</a></li>
						<li><a href="error-404.html">Error 404</a>
							<ul>
								<li><a href="error-404.html">Error 404-1</a></li>
								<li><a href="error-404-1.html">Error 404-2</a></li>
								<li><a href="error-404-2.html">Error 404-3</a></li>
								<li><a href="error-404-3.html">Error 404-4</a></li>
							</ul>
						</li>
						<li><a href="coming-soon.html">Coming Soon</a></li>
					</ul>
				</li>
				<li>
					<a href="blog-classic.html">Blog</a>
					<ul>
						<li><a href="blog-classic.html">Classic</a></li>
						<li><a href="blog-listing.html">Listing</a></li>
						<li>
							<a href="#">Grid</a>
							<ul>
								<li><a href="blog-grid-2col.html">Grid 2 columns</a></li>
								<li><a href="blog-grid-3col.html">Grid 3 columns</a></li>
								<li><a href="blog-grid-4col.html">Grid 4 columns</a></li>
								<li><a href="blog-grid-sidebar.html">Grid sidebar</a></li>
							</ul>
						</li>
						<li>
							<a href="#">Masonry</a>
							<ul>
								<li><a href="blog-masonry-2col.html">Masonry 2 columns</a></li>
								<li><a href="blog-masonry-3col.html">Masonry 3 columns</a></li>
								<li><a href="blog-masonry-4col.html">Masonry 4 columns</a></li>
								<li><a href="blog-masonry-sidebar.html">Masonry sidebar</a></li>
							</ul>
						</li>
						<li>
							<a href="#">Mask</a>
							<ul>
								<li><a href="blog-mask-grid.html">Blog mask grid</a></li>
								<li><a href="blog-mask-masonry.html">Blog mask masonry</a></li>
							</ul>
						</li>
						<li>
							<a href="post-single.html">Single Post</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="element.html">Elements</a>
					<ul>
						<li>
							<a href="#">Elements 1</a>
							<ul>
								<li><a href="element-accordions.html">Accordions</a></li>
								<li><a href="element-alerts.html">Alert &amp; Notification</a></li>

								<li><a href="element-banner-effect.html">Banner Effect
										 
									</a></li>
								<li><a href="element-banner.html">Banner
										 </a></li>
								<li><a href="element-blog-posts.html">Blog Posts</a></li>
								<li><a href="element-breadcrumb.html">Breadcrumb
										 </a></li>
								<li><a href="element-buttons.html">Buttons</a></li>
								<li><a href="element-cta.html">Call to Action</a></li>
								<li><a href="element-countdown.html">Countdown
										 </a></li>
							</ul>
						</li>
						<li>
							<a href="#">Elements 2</a>
							<ul>
								<li><a href="element-counter.html">Counter </a></li>
								<li><a href="element-creative-grid.html">Creative Grid
										 
									</a></li>
								<li><a href="element-animation.html">Entrance Effect
										 
									</a></li>
								<li><a href="element-floating.html">Floating
										 
									</a></li>
								<li><a href="element-hotspot.html">Hotspot
										 
									</a></li>
								<li><a href="element-icon-boxes.html">Icon Boxes</a></li>
								<li><a href="element-icons.html">Icons</a></li>
								<li><a href="element-image-box.html">Image box
										 
									</a></li>
								<li><a href="element-instagrams.html">Instagrams</a></li>

							</ul>
						</li>
						<li>
							<a href="#">Elements 3</a>
							<ul>

								<li><a href="element-categories.html">Product Category</a></li>
								<li><a href="element-products.html">Products</a></li>
								<li><a href="element-product-banner.html">Products + Banner
										 
									</a></li>
								<li><a href="element-product-grid.html">Products + Grid
										 
									</a></li>
								<li><a href="element-product-single.html">Product Single
										 
									</a>
								</li>
								<li><a href="element-product-tab.html">Products + Tab
										 
									</a></li>
								<li><a href="element-single-product.html">Single Product
										 
									</a></li>
								<li><a href="element-slider.html">Slider
										 
									</a></li>
								<li><a href="element-social-link.html">Social Icons </a></li>
							</ul>
						</li>
						<li>
							<a href="#">Elements 4</a>
							<ul>
								<li><a href="element-subcategory.html">Subcategory
										 
									</a></li>
								<li><a href="element-svg-floating.html">Svg Floating
										 
									</a></li>
								<li><a href="element-tabs.html">Tabs</a></li>
								<li><a href="element-testimonials.html">Testimonials
										 
									</a></li>
								<li><a href="element-titles.html">Title</a></li>
								<li><a href="element-typography.html">Typography</a></li>
								<li><a href="element-vendor.html">Vendor
										 
									</a></li>
								<li><a href="element-video.html">Video
										 
									</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="https://d-themes.com/buynow/riodehtml">Buy Riode!</a></li>
			</ul>
			<!-- End MobileMenu -->
		</div>
	</div>
	<!-- sticky icons-->
	<div class="sticky-icons-wrapper">
		<div class="sticky-icon-links">
			<ul>
				<li><a href="#" class="demo-toggle"><i class="fas fa-home"></i><span>Demos</span></a></li>
				<li><a href="documentation.html"><i class="fas fa-info-circle"></i><span>Documentation</span></a>
				</li>
				<li><a href="https://themeforest.net/downloads/"><i class="fas fa-star"></i><span>Reviews</span></a>
				</li>
				<li><a href="https://d-themes.com/buynow/riodehtml"><i class="fas fa-shopping-cart"></i><span>Buy
							now!</span></a></li>
			</ul>
		</div>
		<div class="demos-list">
			<div class="demos-overlay"></div>
			<a class="demos-close" href="#"><i class="close-icon"></i></a>
			<div class="demos-content scrollable scrollable-light">
				<h3 class="demos-title">Demos</h3>
				<div class="demos">
				</div>
			</div>
		</div>
	</div>
	<!-- Plugins JS File -->
	<script src="frontend/vendor/jquery/jquery.min.js"></script>
	<script src="frontend/vendor/sticky/sticky.min.js"></script>
	<script src="frontend/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
	<script src="frontend/vendor/elevatezoom/jquery.elevatezoom.min.js"></script>
	<script src="/frontend/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="frontend/vendor/owl-carousel/owl.carousel.min.js"></script>
	<script src="frontend/vendor/nouislider/nouislider.min.js"></script>
	<!-- Main JS File -->
	<script src="frontend/js/main.min.js"></script>
</body>

</html>